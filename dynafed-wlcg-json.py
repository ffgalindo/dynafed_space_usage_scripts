#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Temporary script to generate a minimal version of the WLCG storage report.

Change the variables below to adjust your settings and the endpoints that need
to be contacted and under which storage shares they belong too.

IMPORTANT: the 'config' variable needs to be a list, otherwise execution will
will fail. You can specify here a paths to directories or files themselves.
Include every configuration file that holds the information for all dynafed
endpoints that will need to be contacted.

These endpoint names are the same ID names used in UGR's configuration files.

Make sure the dynafed_storagestats python package is installed. After that
you should be able to run this script without arguments and it will generate
the file in the output_file path.

"""
## Configuration variables.

dynafed_hostname = 'your.dynafed.com'
config = ['/etc/ugr/conf.d']
logfile = '/dev/null'
loglevel = 'ERROR' #WARNING, INFO, DEBUG
output_file = '/tmp/space-usage.json '

storage_shares = [
    {
        'name': 'ATLASSCRATCHDISK',
        'endpoints': ['ENDPOINT01_ID', 'ENDPOINT02_ID''],
        'path': ['/dynafed/atlas/atlasscratchdisk']
    },
    {   'name': "ATLASDATADISK",
        'endpoints': ['ENDPOINT03_ID', 'ENDPOINT04_ID'],
        'path': ['/dynafed/atlas/atlasdatadisk']
    }
]

## Code
import json
import time

from dynafed_storagestats import configloader
from dynafed_storagestats.helpers import setup_logger

# Starting the logger instance
setup_logger(loglevel=loglevel, logfile=logfile)

# Getting current timestamp
NOW = int(time.time())

# Calculate the totals space and used space for each of the dynafed endpoints
# under each storage share.
for storage_share in storage_shares:
    bytes_used = 0
    total_size = 0
    for endpoint in storage_share['endpoints']:
        dynafed_share = configloader.get_storage_shares(config, endpoint)[0]
        dynafed_share.get_storagestats()
        bytes_used += dynafed_share.stats['bytesused']
        total_size += dynafed_share.stats['quota']

    storage_share.update(
        {
            "totalsize": total_size,
            "timestamp": NOW,
            "usedsize": bytes_used,
        }
    )
    del storage_share['endpoints']

# Create the json structure
skeleton = {
    'storage_service': {
        'latestupdate': NOW,
        'name': dynafed_hostname,
        'storage_shares': storage_shares
    }
}

# Ouptut json to file.
with open(output_file, 'w') as json_file:
    json.dump(skeleton, json_file, indent=4, sort_keys=True)
